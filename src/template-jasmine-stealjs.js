"use strict";

var template = __dirname + '/templates/jasmine-requirejs.html',
    steal  = {
        '0.3.1' : __dirname + '/../../../vendor/steal'
    },
    path = require('path'),
    parse = require('./lib/parse');

function filterGlobPatterns(scripts) {
  Object.keys(scripts).forEach(function (group) {
    if (Array.isArray(scripts[group])) {
      scripts[group] = scripts[group].filter(function(script) {
        return script.indexOf('*') === -1;
      });
    } else {
      scripts[group] = [];
    }
  });
}

function resolvePath(filepath) {
  filepath = filepath.trim();
  if (filepath.substr(0,1) === '~') {
    filepath = process.env.HOME + filepath.substr(1);
  }
  return path.resolve(filepath);
}

function moveRequireJs(grunt, task) {

    task.copyTempFile(steal['0.3.1'] + '/steal.js','steal.js');
    task.copyTempFile(steal['0.3.1'] + '/dev.js','dev.js');
    task.copyTempFile(steal['0.3.1'] + '/less.js','less.js');
    task.copyTempFile(steal['0.3.1'] + '/css.js','css.js');
    task.copyTempFile(steal['0.3.1'] + '/main.js','main.js');
    task.copyTempFile(steal['0.3.1'] + '/system-format-steal.js','system-format-steal.js');
    task.copyTempFile(steal['0.3.1'] + '/less-1.7.0.js','less-1.7.0.js');
    task.copyTempFile(steal['0.3.1'] + '/../../cream.config.js','cream.config.js');
}

exports.process = function(grunt, task, context) {



  // Remove glob patterns from scripts (see https://github.com/gruntjs/grunt-contrib-jasmine/issues/42)
  filterGlobPatterns(context.scripts);

  /**
   * Find and resolve specified baseUrl.
   */
  function getBaseUrl(baseUrl) {
    baseUrl = baseUrl || context.options.requireConfig && context.options.requireConfig.baseUrl || '.';
    return grunt.file.expand({
        filter: 'isDirectory',
        cwd: path.dirname(path.join(process.cwd(), context.outfile))
    }, baseUrl)[0] || getBaseUrl('.');
  }
  var baseUrl = getBaseUrl();

  /**
   * Retrieves the module URL for a require call relative to the specified Base URL.
   */
  function getRelativeModuleUrl(src) {
    return path.relative(baseUrl, src).replace(/\.js$/, '');
  }

  // Remove baseUrl and .js from src files
  context.scripts.src = grunt.util._.map(context.scripts.src, getRelativeModuleUrl);


  // Prepend loaderPlugins to the appropriate files
  if (context.options.loaderPlugin) {
    Object.keys(context.options.loaderPlugin).forEach(function(type){
      if (context.scripts[type]) {
        context.scripts[type].forEach(function(file,i){
          context.scripts[type][i] = context.options.loaderPlugin[type] + '!' + file;
        });
      }
    });
  }

  moveRequireJs(grunt, task);

  context.serializeRequireConfig = function(requireConfig) {
    var funcCounter = 0;
    var funcs = {};

    function isUnserializable(val) {
      var unserializables = [Function, RegExp];
      var typeTests = unserializables.map(function(unserializableType) {
        return val instanceof unserializableType;
      });
      return !!~typeTests.indexOf(true);
    }

    function generateFunctionId() {
      return '$template-jasmine-require_' + new Date().getTime() + '_' + (++funcCounter);
    }

    var jsonString = JSON.stringify(requireConfig, function(key, val) {
      var funcId;
      if (isUnserializable(val)) {
        funcId = generateFunctionId();
        funcs[funcId] = val;
        return funcId;
      }
      return val;
    }, 2);

    Object.keys(funcs).forEach(function(id) {
      jsonString = jsonString.replace('"' + id + '"', funcs[id].toString());
    });

    return jsonString;
  };

  // update relative path of .grunt folder to the location of spec runner
  /*context.temp = path.relative(path.dirname(context.outfile),
                               context.temp);*/

  var source = grunt.file.read(template);
  return grunt.util._.template(source, context);
};